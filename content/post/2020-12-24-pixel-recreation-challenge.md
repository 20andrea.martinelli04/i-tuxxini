---
title: Pixel Recreation Challenge
date: 2021-01-13
---
{{< gviewer 1i0kTYxfVQPRsgTlg590cAqe0oGhZnW84Xir2nJB6CyA >}}

Dei componenti della società dei Tuxxini si sono impegnati a realizzare una pixel-art della "Monna Lisa", ottenendo un ottimo risultato. Ogni componente del Team ha contribuito al progetto impegandosi al massimo. Nonostante le distanze dovute al emergenza del Covid-19, che ha reso difficoltosa la conclusione del progetto, il Team è riuscito a concludere il lavoro, incontrandosi online per potersi confrontare e lavorare nel migliore dei modi. 

A few members of the Tuxxini company have undertaken to relay a pixel-art of "Monna Lisa", obtaining an excellent result. Each member of the Team has contributed to the project by doing their utmost. Despite the distances due to the Covid-19 emergency, which made it difficult to complete the project, the Team managed to finish the work, meeting online to be able to compare and work in the best possible way.

Einige Mitglieder der Tuxxini Gruppe haben sich verpflichtet, eine Pixel Art ueber der Monna Lisa zu realisieren. Sie haben ein sehr gut Ereignis erzielt. Jeder Mitglieder hat zu dem Projekt viel beigetragen. Trotz den Entfernungen von dem Covid-19 Not, der den Schluss des Projekts schwieriger gemacht hat, konnte der Team die Arbeit beenden auch sich online treffen, um sich zu auseinandersetzen und am besten zu arbeiten.
